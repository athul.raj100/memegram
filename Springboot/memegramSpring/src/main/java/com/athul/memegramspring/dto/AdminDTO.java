package com.athul.memegramspring.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdminDTO {

    private String email;
    private String password;
    private String repeatPassword;
}
