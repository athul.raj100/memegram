package com.athul.memegramspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MemegramSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(MemegramSpringApplication.class, args);
	}

}
